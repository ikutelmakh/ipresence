**ASSIGNMENT #1:**
---

## Automate the following test case using Java and Selenium-WebDriver (or using wrappers) 
   1. Navigate to https://leolist.cc/motors/parts/calgary page 
   2. Change Region to British Columbia -> Metro Vancouver 
   3. Click More Filters button 
   4. Select Has Photos check box 
   5. Select Vancouver checkbox in Cities filter 
   6. Insert "Scrap Car" text into Search input 
   7. Click Show Ads button 
   8. Validate that Ads in the result table contain Scrap Car text 
   9. Validate that Ads in the result table from Vancouver city only 
   10. Validate that Ads in the result table contain photos 
---
## Installed toolset required to run the test

1. Java 8 JDK
2. Git
3. Maven v.3+
4. Chrome

---

## How to run the test

1. Clone current repository
2. Go into the root, where pom.xml file is located
3. Open terminal in the current folder
4. Run "mvn clean test" command

---

## Possibilities to extend the framework

1. Add proper logging
2. Test data provider
3. Multi browser support
4. Test result reporting etc.

*PS:* Video of the test execution is under ${project.dir}/video