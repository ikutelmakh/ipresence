package ca.ipresence;

import ca.ipresence.utility.PropertyManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    WebDriver driver;

    @BeforeMethod
    public void setup() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get(new PropertyManager().appConfig().baseUrl());
    }

    @AfterMethod
    public void teardown() {
        if (driver != null) {
            driver.close();
        }
    }
}
