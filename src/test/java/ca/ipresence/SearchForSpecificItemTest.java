package ca.ipresence;

import ca.ipresence.pages.CategoryPage;
import ca.ipresence.pages.ResultSetPage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SearchForSpecificItemTest extends BaseTest {

    @Test(description = "Search for the scrap cars in Vancouver city")
    void searchForScrapCars() {

        //Set test data
        String regionName = "British Columbia";
        String areaName = "Metro Vancouver";
        String searchItem = "Scrap Car";
        String city = "Vancouver";

        //Perform the search
        ResultSetPage results =
                new CategoryPage(driver)
                        .chooseTheRegion(regionName, areaName)
                        .applyHasPhotosFilter()
                        .selectTheCity(city)
                        .search(searchItem)
                        .showAds();

        // Validate that Ads in the result table contain Scrap Car text
        assertTrue(results.checkItemTitle(searchItem));

        // Validate that Ads in the result table contain photos
        assertTrue(results.checkItemImage());

        // Validate that Ads in the result table from Vancouver city only
        assertTrue(results.checkItemCity(city));
    }
}
