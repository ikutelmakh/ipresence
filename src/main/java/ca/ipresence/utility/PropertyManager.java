package ca.ipresence.utility;

import org.aeonbits.owner.ConfigFactory;

public class PropertyManager {

    /**
     * Get a base app configuration from the property file.
     *
     * @return
     */
    public AppConfig appConfig() {

        AppConfig appConfig = ConfigFactory.create(AppConfig.class);
        return appConfig;
    }
}
