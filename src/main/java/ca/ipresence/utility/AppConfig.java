package ca.ipresence.utility;

import org.aeonbits.owner.Config;

@Config.Sources("classpath:app.config.properties")
public interface AppConfig extends Config {

    @Key("BASE.URL")
    String baseUrl();
}
