package ca.ipresence.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CategoryPage extends BasePage {

    @FindBy(xpath = "//div[@id='view']/div/div")
    private WebElement regionDropdown;

    @FindBy(xpath = "//form[@id='form-search']/input")
    private WebElement searchField;

    @FindBy(id = "more-filters")
    private WebElement moreFiltersButton;

    @FindBy(xpath = "//label[contains(text(),'Has Photos')]")
    private WebElement hasPhotosFilterCheckbox;

    @FindBy(xpath = "//button[contains(text(),'Show Ads')]")
    private WebElement showAdsButton;

    public CategoryPage(WebDriver driver) {
        super(driver);
    }

    WebDriverWait wait = new WebDriverWait(driver, 5);

    /**
     * Change the default location to specific one.
     *
     * @param regionName Name of region
     * @param areaName   Name of area in specific region
     * @return
     */
    public CategoryPage chooseTheRegion(String regionName, String areaName) {

        regionDropdown.click();

        WebElement region = wait
                .until(ExpectedConditions.visibilityOfElementLocated(By
                        .xpath("//div[contains(text(), '" + regionName + "')]")));
        region.click();

        WebElement area = wait
                .until(ExpectedConditions.visibilityOfElementLocated(By
                        .xpath("//a[contains(text(), '" + areaName + "')]")));
        area.click();
        return this;
    }

    /**
     * Apply a 'Has Photos' filter to the search
     *
     * @return
     */
    public CategoryPage applyHasPhotosFilter() {

        moreFiltersButton.click();
        hasPhotosFilterCheckbox.click();

        return this;
    }

    /**
     * Select specific city for a search.
     *
     * @param city City for the search
     * @return
     */
    public CategoryPage selectTheCity(String city) {

        WebElement cityElement = wait
                .until(ExpectedConditions.visibilityOfElementLocated(By
                        .xpath("//label[contains(text(),'" + city + "')]")));
        cityElement.click();
        return this;
    }

    /**
     * Free text of the item that needs be found.
     *
     * @param searchItem Free text for search
     * @return
     */
    public CategoryPage search(String searchItem) {

        searchField.sendKeys(searchItem);

        return this;
    }

    /**
     * Perform a search by clicking on the button.
     *
     * @return
     */
    public ResultSetPage showAds() {

        showAdsButton.click();
        return new ResultSetPage(driver);
    }
}
