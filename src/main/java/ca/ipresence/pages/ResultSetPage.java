package ca.ipresence.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class ResultSetPage extends CategoryPage {

    @FindBy(xpath = "//div[@class='group']")
    private List<WebElement> resultSet;

    public ResultSetPage(WebDriver driver) {
        super(driver);
    }

    WebDriverWait wait = new WebDriverWait(driver, 5);

    /**
     * Check if returned for the search item contains needed text.
     *
     * @param titleShouldContain String. Text that returned item should contain in title
     * @return
     */
    public Boolean checkItemTitle(String titleShouldContain) {

        Boolean stringContains = false;

        wait.until(ExpectedConditions.visibilityOfAllElements(resultSet));

        for (WebElement element : resultSet) {
            if (element.getText().contains(titleShouldContain)) {
                stringContains = true;
            }
        }
        return stringContains;
    }

    /**
     * Check if returned for the search item contains image instead of default placeholder.
     *
     * @return
     */
    public Boolean checkItemImage() {

        Boolean imageIsVisible = false;
        wait.until(ExpectedConditions.visibilityOfAllElements(resultSet));

        List<WebElement> images = new ArrayList<>();

        images.addAll(driver.findElements(By.xpath("//img[@class='thumb']")));

        for (WebElement image : images) {
            if (image.isDisplayed()) {
                imageIsVisible = true;
            }
        }
        return imageIsVisible;
    }

    /**
     * Check if all returned items are for the correct location (city).
     *
     * @param cityName City, for what search is made
     * @return
     */
    public Boolean checkItemCity(String cityName) {

        Boolean isTheOnlyCity = false;
        wait.until(ExpectedConditions.visibilityOfAllElements(resultSet));

        List<WebElement> itemCities = new ArrayList<>();
        itemCities.addAll(driver.findElements(By.xpath("//span[@class='desc__info']")));

        for (WebElement cities : itemCities) {
            if (cities.getText().contains(cityName)) {
                isTheOnlyCity = true;
            }
        }
        return isTheOnlyCity;
    }
}
